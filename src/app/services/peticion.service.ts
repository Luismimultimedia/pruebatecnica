import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interface/user';
import { Credentials } from '../interface/credentials';
import { Registers } from '../interface/registers';

import { map } from 'rxjs/operators';
import { reject } from 'q';



@Injectable({
  providedIn: 'root'
})
export class PeticionService {

  urlBase = '/olsoftware_pruebas/usersService';
  queryLogin = '/Login';
  queryGetUsers = '/SearchUser';
  queryAddUsers = '/SaveUser';
  queryDelateUsers = '/DeleteUser';

  constructor(private http: HttpClient) { }

//   loginService(credentials: Credentials) {
//     const body = JSON.stringify(credentials);
//     const url = `${this.urlBase}${this.queryLogin}`;
//     const headers = new HttpHeaders({'Content-Type': 'application/json'});
//     headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
//     headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
//     headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     headers.append('Access-Control-Allow-Credentials', 'true');

//     return this.http.post(url, body, {headers})
//     .pipe(map( data => {
//       console.log(data);
//       return data;
//       }));
// }

loginService(params: Credentials): Promise<any> {
    const body = JSON.stringify(params);
    const url = `${this.urlBase}${this.queryLogin}`;
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    headers.append('Access-Control-Allow-Credentials', 'true');

    return this.http
        .post(url, body, {headers})
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);
}

getUsersService(params: Registers) {
const body = JSON.stringify(params);
const url = `${this.urlBase}${this.queryGetUsers}`;
const headers = new HttpHeaders({'Content-Type': 'application/json'});
headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
headers.append('Access-Control-Allow-Credentials', 'true');

return this.http
.post(url, body, {headers} )
.pipe(map(data => {
  console.log(data);
  return data;
}));

}

addUser(params: User): Promise<any> {
  const body = JSON.stringify(params);
  const url = `${this.urlBase}${this.queryAddUsers}`;
  const headers = new HttpHeaders({'Content-Type': 'application/json'});
  headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
  headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  headers.append('Access-Control-Allow-Credentials', 'true');

  return this.http
      .post(url, body, {headers})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
}

delateUser(params: {}) {
  const body = JSON.stringify(params);
  const url = `${this.urlBase}${this.queryDelateUsers}`;
  const headers = new HttpHeaders({'Content-Type': 'application/json'});
  headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
  headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS');
  headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  headers.append('Access-Control-Allow-Credentials', 'true');

  return this.http
      .post(url, body, {headers})
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
}

private extractData(res: Response) {
  // tslint:disable-next-line:prefer-const
  let body = res;
  return body || {};
}

private handleError(error: any): Promise<any> {
  console.error('An error occurred', error);
  return Promise.reject(error.message || error);
}

}
