import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { UsersComponent } from './components/users/users.component';
import { LoadingComponent } from './components/loading/loading.component';

import { PeticionService } from './services/peticion.service';


import { RolUserPipe } from './pipes/rol-user.pipe';
import { EstadoUserPipe } from './pipes/estado-user.pipe';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    UsersComponent,
    LoadingComponent,
    RolUserPipe,
    EstadoUserPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    PeticionService,
    NavbarComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
