import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Credentials } from '../../interface/credentials';
import { PeticionService } from '../../services/peticion.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  master: Credentials = {
    login: '',
    pass: ''
  };

  resultado: any = {
    codigoError : '',
    mensajerError: ''
  };

  loading: boolean;

  loginState: boolean;

  constructor(private _peticionService: PeticionService,
    private router: Router) { }

  ngOnInit() {
    document.body.classList.add('bg-img');
  }

  login() {
    this.loading = true;
    this._peticionService
    .loginService(this.master)
    .then(result => {
      this.loading = false;
      console.log(result);
      this.resultado = result;
      // tslint:disable-next-line:triple-equals
      if (this.resultado.codigoError == 1) {
              console.log(this.resultado.mensajeError);
            } else {
              this.loginState = true;
              this.router.navigate(['/users/1']);
            }
    })
    .catch(error => console.log(error));
  }
}
