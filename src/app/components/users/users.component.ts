import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Registers } from '../../interface/registers';
import { PeticionService } from '../../services/peticion.service';
import { User } from '../../interface/user';
import { Router } from '@angular/router';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {


  @ViewChild('forma')
  form: NgForm;

  registro: Registers = {
    inicioFila: 0,
    numRegistros: 8
  };

  usuario: User = {
    numId: 0,
    nombres: '',
    apellidos: '',
    rol: 0,
    estado: '',
    telefono: 0,
    correo: '',
    contrasena: ''
  };

  borrar = {
    numId: 0
  };

  resultadoUsuarios: any[] = [];
  statedUser: boolean;
  statedUpdateUser: boolean;
  inicioPaginacion: number[] = [0];
  abiertoSource: NavbarComponent;

  @Input()
  abierto = true;

  constructor(private _peticionService: PeticionService,
    private router: Router) {
      this.showUsers(this.registro);
      this.showPagination();
      this.statedUser = false;
      this.statedUpdateUser = false;
   }

  ngOnInit() {
    document.body.classList.remove('bg-img');
    this.colpaseMenu(this.abierto);
  }

  showUsers(regitro: any) {
    this._peticionService
    .getUsersService(regitro)
    .subscribe((data: any) => {
      this.resultadoUsuarios = data;
    });
  }

  sendUser() {
    this._peticionService
    .addUser(this.usuario)
    .then( result => {
      console.log('usuarioAgregado');
      this.showUsers(this.registro);
      this.showPagination();
      this.statedUser = false;
      this.statedUpdateUser = false;
      this.router.navigate(['/users/1']);
    })
    .catch(error => {
      console.log(error);
      alert(error);
    });
  }


  delate(param: any) {
    this.borrar.numId = param;
    this._peticionService
    .delateUser(this.borrar)
    .then( result => {
      console.log('usuarioBorrado');
      this.showUsers(this.registro);
      this.showPagination();
      this.router.navigate(['/users/1']);
    })
    .catch(error => console.log(error));
  }

  createUser(form: NgForm) {
    this.statedUser = true;
    form.reset({
      nombres: '',
      apellidos: '',
      numId: '',
      rol: '',
      estado: '',
      contrasena: '',
      telefono: '',
      correo: ''
    });
  }

  updateUser(form: NgForm, usuario: User) {
    this.usuario.numId = usuario.numId;
    this.usuario.nombres = usuario.nombres;
    this.usuario.apellidos = usuario.apellidos;
    this.usuario.rol = usuario.rol;
    this.usuario.estado = usuario.estado;
    this.usuario.telefono = usuario.telefono;
    this.usuario.correo = usuario.correo;
    this.usuario.contrasena = usuario.contrasena;
    form.setValue({
        nombres: this.usuario.nombres,
        apellidos: this.usuario.apellidos,
        numId: this.usuario.numId,
        rol: this.usuario.rol,
        estado: this.usuario.estado,
        contrasena: this.usuario.contrasena,
        telefono: this.usuario.telefono,
        correo: this.usuario.correo
    });
  }

  showPagination() {
    const REGISTRO: Registers = {
      inicioFila: 0,
      numRegistros: 100
    };
    this._peticionService
    .getUsersService(REGISTRO)
    .subscribe((data: any) => {
      console.log(data);
      const usuariosTotales = data;
      for (let index = 0; index < usuariosTotales.length; index++) {
        if (index >= 8) {
            // tslint:disable-next-line:triple-equals
            if ((index % 8) == 0) {
             this.inicioPaginacion.push(index);
             console.log(this.inicioPaginacion);
            }
        }
      }
    });
  }

  colpaseMenu(abierto: boolean) {
    if (abierto) {
      document.getElementById('mySidenav').style.width = '280px';
      document.getElementById('user-page').style.marginLeft = '300px';
      document.getElementById('item-menu1').style.display = 'inline-block';
      document.getElementById('item-menu2').style.display = 'inline-block';
      document.getElementById('item-menu3').style.display = 'inline-block';
      document.getElementById('item-menu4').style.display = 'inline-block';
      document.getElementById('item-menu5').style.display = 'inline-block';
      document.getElementById('item-menu6').style.display = 'inline-block';
      document.getElementById('item-menu7').style.display = 'inline-block';
      document.getElementById('icon1').style.display = 'inline-block';
      document.getElementById('icon2').style.display = 'inline-block';
      document.getElementById('icon3').style.display = 'inline-block';
      document.getElementById('icon4').style.display = 'inline-block';
    } else {
      document.getElementById('mySidenav').style.width = '70px';
      document.getElementById('user-page').style.marginLeft = '90px';
      document.getElementById('item-menu1').style.display = 'none';
      document.getElementById('item-menu2').style.display = 'none';
      document.getElementById('item-menu3').style.display = 'none';
      document.getElementById('item-menu4').style.display = 'none';
      document.getElementById('item-menu5').style.display = 'none';
      document.getElementById('item-menu6').style.display = 'none';
      document.getElementById('item-menu7').style.display = 'none';
      document.getElementById('icon1').style.display = 'none';
      document.getElementById('icon2').style.display = 'none';
      document.getElementById('icon3').style.display = 'none';
      document.getElementById('icon4').style.display = 'none';
    }
}

cambiarPaginacion(idx: number, item: number) {
  this.registro.inicioFila = item;
  this.showUsers(this.registro);
  this.router.navigate(['/users', idx]);
}

}
