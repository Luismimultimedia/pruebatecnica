export interface Credentials {
    login: string;
    pass: string;
}
