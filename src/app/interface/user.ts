export interface User {
    numId: number;
    nombres: string;
    apellidos: string;
    rol: number;
    estado: string;
    telefono: number;
    correo: string;
    contrasena?: string;
}
