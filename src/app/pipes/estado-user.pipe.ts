import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estadoUser'
})
export class EstadoUserPipe implements PipeTransform {

  transform(estado: string): any {
    // tslint:disable-next-line:triple-equals
    if (estado == 'I') {
      return 'Inactivo';
    // tslint:disable-next-line:triple-equals
    }if (estado == 'A') {
      return 'Activo';
    }
  }
}
