import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rolUser'
})
export class RolUserPipe implements PipeTransform {

  transform(rol: number): any {
    // tslint:disable-next-line:triple-equals
    if (rol == 1) {
      return 'Administrador';
    // tslint:disable-next-line:triple-equals
    }if (rol == 2) {
      return 'Conductor';
    // tslint:disable-next-line:triple-equals
    }if (rol == 3) {
      return 'Recolector';
    }
  }

}
